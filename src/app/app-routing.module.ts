import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { LoginComponent } from './modules/home/pages/login/login.component';
import { BoardsComponent } from './main/components/boards/boards.component';
import { BoardTableComponent} from './main/components/board-table/board-table.component';
import { StudyTableComponent} from './main/components/study-table/study-table.component';

const routes: Routes = [
 {path: '', redirectTo: 'boards', pathMatch: 'full'},
 {path: 'login', component: LoginComponent},
 {path: 'boards', component: BoardsComponent, canActivate: [AuthGuard]},
 {path: 'board-table', component: BoardTableComponent, canActivate: [AuthGuard]},
 {path: 'study-table', component: StudyTableComponent, canActivate: [AuthGuard]},
];

@NgModule({
 imports: [
   RouterModule.forRoot(routes, {useHash: false, scrollPositionRestoration: 'top'}),
 ],
 exports: [
   RouterModule
 ]
})

export class AppRoutingModule {
 constructor() { }
}