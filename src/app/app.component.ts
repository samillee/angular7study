import {Component, OnInit} from '@angular/core';
import {DaoService} from './core/http/dao.service';
import {map} from 'rxjs/operators';

@Component({
 selector: 'app-root',
 templateUrl: './app.component.html',
 styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
 title = 'ngnote-example';
 boards: any;

 constructor(private daoService: DaoService) {
 }

 ngOnInit() {
   this.getBoardsList();
 }

 getBoardsList() {
   this.daoService.getBoardsList().snapshotChanges().pipe(
     map(changes =>
       changes.map(c => ({key: c.payload.key, ...c.payload.val()}))
     )
   ).subscribe(boards => {
     this.boards = boards;
   });
 }

 deleteCustomers() {
   this.daoService.deleteAll();
 }
}