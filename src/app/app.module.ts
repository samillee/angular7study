import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { MatCheckboxModule } from '@angular/material';

import { FormsModule } from '@angular/forms';
import { MaterialModule } from './shared/modules/material/material.module';
import { FirebaseModule } from './shared/modules/firebase/firebase.module';
import { AuthGuard } from './core/guards/auth.guard';
import { LoginComponent } from './modules/home/pages/login/login.component';
import { NavComponent } from './core/header/nav.component';
import { FooterComponent } from './core/footer/footer.component';
import { BoardsComponent } from './main/components/boards/boards.component';
import { WriteFeedComponent } from './main/components/write-feed/write-feed.component';
import { FeedComponent } from './main/components/feed/feed.component';
import { BoardTableComponent } from './main/components/board-table/board-table.component';
import { StudyTableComponent } from './main/components/study-table/study-table.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavComponent,
    FooterComponent,
    BoardsComponent,
    WriteFeedComponent,
    FeedComponent,
    BoardTableComponent,
    StudyTableComponent
  ],
  imports: [
    // BrowserModule,
    // BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    MaterialModule,
    FirebaseModule
  ],
  providers: [
    AuthGuard
    // AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
