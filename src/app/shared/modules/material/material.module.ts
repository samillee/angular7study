import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { 
    MatAutocompleteModule,
    MatButtonModule, 
    MatCardModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatInputModule,
    MatMenuModule, 
    MatPaginatorModule,
    MatSortModule, 
    MatTableModule, 
   } from '@angular/material';

@NgModule({
  imports: [
    // CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatButtonModule, 
    MatCardModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatInputModule,
    MatMenuModule, 
    MatPaginatorModule,
    MatSortModule, 
    MatTableModule, 
  ],
  exports: [
    // CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatButtonModule, 
    MatCardModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatInputModule,
    MatMenuModule, 
    MatPaginatorModule,
    MatSortModule, 
    MatTableModule, 
  ],
  declarations: []
})
export class MaterialModule { }
